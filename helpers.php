<?php

function faktorialas($skaicius) {
	$rezultatas = 1;

	for($i = 1; $i <= $skaicius; $i++) {
		$rezultatas *= $i;
	}

	return $rezultatas;
}


function arKeliamiejiMetai($metai) {
	if ($metai % 400 == 0 || ($metai % 100 != 0 && $metai % 4 == 0)) {
		return true;
	} else {
		return false;
	}
}