<?php include('header.php'); ?>

<?php

$rockBands = array(
array('Beatles','Love Me Do', 'Hey Jude','Helter Skelter'), 
array('Rolling Stones','Waiting on a Friend','Angie',
'Yesterday\'s Papers'), 
array('Eagles','Life in the Fast Lane','Hotel California',
'Best of My Love'));

?>

<table>
	<tr>
		<th>
			<a href="?sort=name&order=asc">Band name</a>
		</th>
		<th><a href="?sort=song">Band name</a></th>
		<th>Song</th>
		<th>Song</th>

	</tr>
	<?php foreach($rockBands as $band): ?>
		<tr>
			<?php foreach($band as $song) : ?>
				<td>
					<?php echo $song; ?>
				</td>
			<?php endforeach; ?>
		</tr>
	<?php endforeach; ?>
</table>