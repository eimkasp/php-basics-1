<?php 


$duomenys = $_GET['duomenys']; // 35, 36

$duomenysMasyvas = explode(',', $duomenys); // masyvas

$laikai = [];

foreach ($duomenysMasyvas as $key => $temepratura) {
	$laikai[] = $key + 8 . " h";
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Duomenys</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.js"></script>
</head>
<body>
<h1>Ligionio duomenys </h1>

<table>
	<?php foreach($duomenysMasyvas as $temperatura) : ?>
		<tr>
			<td>Temperatura: </td>
			<td> <?php echo $temperatura; ?> </td>
		</tr>
	<?php endforeach; ?>

</table>
<div style="margin: 0 auto; width: 500px;">
	<canvas id="myChart" width="400" height="400"></canvas>
</div>

<div id="duomenys" data-masyvas="<?php echo json_encode($duomenysMasyvas); ?>"></div>

<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function() {
		var ctx = document.getElementById('myChart').getContext('2d');
		var myChart = new Chart(ctx, {
		    type: 'line',
		    data: {
		        labels: <?php echo json_encode($laikai); ?>,
		        datasets: [{
		            label: 'Temperatura',
		            data: <?php echo json_encode($duomenysMasyvas); ?>,
		            backgroundColor: 'rgba(255, 99, 132, 0.2)'
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});
	});
	
</script>
</body>
</html>





