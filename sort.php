<?php

 function bestSortEver($a, $b) {
  global $order;
  global $x;
  if($order == "ASC") {
    if ($a[$x] < $b[$x]) {
      return false;
    } else {
      return true;
    }
  } else {
    if ($a[$x] > $b[$x]) {
        return false;
      } else {
        return true;
      }  
   }
  }



 $projects = array(
  array('id' => '1','short_name' => 'BIO-C3','year' => '2014','program' => 'BONUS','price' => '3700000'),
  array('id' => '2','short_name' => 'TRIPOLIS','year' => '2014','program' => 'LMT','price' => '79385'),
  array('id' => '3','short_name' => 'LADA','year' => '2015','program' => 'BONUS','price' => '2868208'),
  array('id' => '4','short_name' => 'NORD','year' => '2013','program' => 'LMT','price' => '54446'),
  array('id' => '6','short_name' => 'EAST','year' => '2015','program' => 'BRAVO','price' => '40000'),
  array('id' => '7','short_name' => 'LTLT','year' => '2014','program' => 'FDG','price' => '5000')
 );
//print_r($projects);
//uasort($projects, 'so');
?>
<?php
if (isset($_GET['sort'])) {
 $x = $_GET['sort'];
 $order = $_GET['order'];
} else {
 $x = '';
 $order = "ASC";
}

 uasort($projects, 'bestSortEver');



function getOrder() {
  global $order;
  if($order == "ASC") {
    return "DESC";
  } else {
    return "ASC";
  }
}

?>
<table class="table table-bordered table-hover" style="width: 500px; margin:100px;">
 <tr>
  <th>Vykdomi projektai</th>
 </tr>
 <tr>
  <th><a href="?sort=short_name&order=<?php echo getOrder(); ?>">Pavadinimas</a></th>
  <th><a href="?sort=year&order=<?php echo getOrder();?>">Metai</a></th>
  <th><a href="?sort=program&order=<?php echo getOrder(); ?>">Programa</a></th>
  <th><a href="?sort=price&order=<?php echo getOrder();?>">Suma</a></th>
 </tr>
  <?php foreach ($projects as $k => $id): ?>
 <tr>
  <td><?php echo $id['short_name']; ?></td>
  <td><?php echo $id['year']; ?></td>
  <td><?php echo $id['program']; ?></td>
  <td><?php echo $id['price']; ?></td>
 </tr>
 <?php endforeach; ?>
</table><hr>
 <?php
 $pagalMetus = array();
 $sumaPagalmetus = array();
 ?>
 <?php foreach ($projects as $key => $metai): ?>
  <?php
    $id = $metai['year'];
    $pagalMetus[$id][] = $metai['price'];
    //echo $id;
  ?>
 <?php endforeach; ?>
 <?php foreach ($pagalMetus as $metai => $suma): ?>
  <?php
     $sumaPagalmetus[] = array('year' => $metai, 'price' => array_sum($suma));
   ?>
 <?php endforeach; ?>
<?php //var_dump($sumaPagalmetus);
?>
<table class="table table-bordered table-hover" style="width: 500px; margin:100px;">
 <?php for ($i=0; $i < count($sumaPagalmetus); $i++) : ?>
  <tr>
   <td><?php echo $sumaPagalmetus[$i]['year']; ?></td>
   <td><?php echo $sumaPagalmetus[$i]['price']; ?></td>
  </tr>
 <?php endfor; ?>
</table>