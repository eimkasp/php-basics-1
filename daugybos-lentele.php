<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<table class="table table-striped text-center" style="width: 300px;">
	<tr>
		<?php for($i = 1; $i < 12; $i++): ?>
			<?php if($i == 1) : ?>
				<td>X</td>
			<?php endif; ?>
			<td style="background: green;"><strong> <?php echo $i; ?></strong> </td>
		<?php endfor; ?>
	</tr>
		<?php for($row = 1; $row <= 12; $row++): ?>
			<tr>
				<?php for($col = 1; $col < 12; $col++) : ?>
					<?php if($col == 1):  ?>
						<td style='background: green;'>
							<strong><?php echo $row; ?></strong>
						</td>
					<?php endif; ?>
					<td>
						<?php echo $row * $col; ?>
					</td>
				<?php endfor; ?>
			</tr>
		<?php endfor; ?>
</table>


<?php
$skaicius = rand(1, 55);

echo $skaicius;
 ?>