<?php 
session_start();

$user = ["email" => "123@123.lt", "password" => "3d186804534370c3c817db0563f0e461"];

if(isset($_SESSION['visit_time'])) {
	// $_SESSION['visit_time'] = time();
} else {
	$_SESSION['visit_time'] = time();
}

if(isset($_SESSION['pages_count'])) {
	$_SESSION['pages_count']++;
} else {
	$_SESSION['pages_count'] = 1;
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Pamoka 1</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
</head>
<body>
<div class="alert alert-success">
	Tu apsilankei : <?php echo $_SESSION['pages_count']; ?> puslapiu
</div>

<div class="alert alert-danger">
	Tu praleidai laiko : <?php echo time() - $_SESSION['visit_time']; ?>
</div>
<ul>
	<li><a href="/labas/sesijos.php">Sesijos 1 </a></li>
	<li><a href="/labas/sesijos2.php">Sesijos 2 </a></li>
	<li><a href="/labas/bands.php">Bands</a></li>
	<?php if($_SESSION['logged_in'] != true) : ?>
	<li><a href="/labas/login.php">Prisijungti</a></li>
	<?php else: ?>
	<li><a href="/labas/login.php">Atsijungti</a></li>
	<?php endif; ?>

</ul>

